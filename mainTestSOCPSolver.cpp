#include "GraspMetric/Metric.h"

USE_PRJ_NAMESPACE

template <typename METRIC>
void testSphere(sizeType minPoint,sizeType maxPoint)
{
  sizeType nrPt=RandEngine::randI(minPoint,maxPoint);
  typename METRIC::Pts points,normals;
  for(sizeType i=0; i<nrPt; i++) {
    points.push_back(Vec3d::Random().normalized());
    normals.push_back(points.back());
  }
  Vec3d extf=Vec3d::Random();
  Vec3d extt=Vec3d::Random();
  METRIC m(points,normals,0.7);
  Cold f=m.solve(extf,extt);
  m.testSolution(extf,extt,f);
}
int main()
{
  testSphere<MetricSCS>(3,5);
}
