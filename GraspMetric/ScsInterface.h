#ifndef SCS_INTERFACE_H
#define SCS_INTERFACE_H

#include "CommonFile/MathBasic.h"
#include "CommonFile/solvers/Objective.h"
#include "scs/scs.h"

PRJ_BEGIN

class ScsInterface
{
public:
  enum CONE_TYPE
  {
    EQUALITY,
    INEQUALITY,
    SECOND_ORDER,
    SEMI_DEFINITE,
  };
  typedef Objective<scalarD>::Vec Vec;
  typedef Objective<scalarD>::SMat SMat;
  typedef Objective<scalarD>::STrip STrip;
  typedef Objective<scalarD>::STrips STrips;
  typedef std::map<CONE_TYPE,std::vector<std::pair<SMat,Vec>>> CONES;
  ScsInterface();
  ~ScsInterface();
  //b-A*x \in K
  void constraintLinear(const Matd& coef,sizeType off,const Vec& b,CONE_TYPE type);
  void constraintLinear(const Vec& coef,sizeType off,scalarD b,CONE_TYPE type);
  void constraintSecondOrder(const Matd& coef,sizeType off,const Vec& b,CONE_TYPE type);
  void constraintSecondOrder(const SMat& coef,sizeType off,const Vec& b,CONE_TYPE type);
  void constraintLinearMatrixInequality(const Matd& coef,sizeType off,const Matd& c0,CONE_TYPE type);
  sizeType solve(const Vec& c,Vec& x,bool report=false);
private:
  ScsCone *_k;
  ScsData *_d;
  ScsSolution *_sol;
  CONES _cones;
};

PRJ_END

#endif
