#ifndef DEBUG_GRADIENT_H
#define DEBUG_GRADIENT_H

#define DEFINE_NUMERIC_DELTA_T(T) \
T DELTA=0;  \
if(sizeof(T)==4)    \
  DELTA=1E-5f;  \
else if(sizeof(T)==8)   \
  DELTA=1E-9;   \
else {  \
  ASSERT(sizeof(T)==16) \
  DELTA=1E-15;  \
}
#define DEFINE_NUMERIC_DELTA DEFINE_NUMERIC_DELTA_T(scalar)

//gradient debug
#define DEBUG_GRADIENT(NAME,A,B) \
if(std::abs(B) > std::sqrt(DELTA)) { \
  WARNINGV(NAME ": %lf Err: %.10lf",double(A),double(B)) \
} else {  \
  INFOV(NAME ": %lf Err: %.10lf",double(A),double(B))  \
}
#define DEBUG_GRADIENT_REL(NAME,A,B) \
if(std::abs(B) > std::sqrt(DELTA)*std::abs(A)) { \
  WARNINGV(NAME ": %lf Err: %.10lf",double(A),double(B)) \
} else {  \
  INFOV(NAME ": %lf Err: %.10lf",double(A),double(B))  \
}

#endif
