#ifndef METRIC_H
#define METRIC_H

#include "MosekInterface.h"

PRJ_BEGIN

//assume center-of-mass is origin
//given a set of grasp points and normals
//compute the force-minimizing grasp to resist external forces and torques
//argmin_f ||f||^2
//s.t.     f in frictional cone
//         f matches external force
//         x \times f match external torque
class Metric
{
public:
  DECL_MOSEK_TYPES
  typedef std::vector<Vec3d,Eigen::aligned_allocator<Vec3>> Pts;
  Metric(const Pts& points,const Pts& normals,scalar fcoef);
  virtual Cold solve(const Vec3d& extf,const Vec3d& extt) const;
  virtual scalarD testSolution(const Vec3d& extf,const Vec3d& extt,const Cold& f) const;
  const Vec3d inNormal(sizeType id) const;
  const Vec3d tangent1(sizeType id) const;
  const Vec3d tangent2(sizeType id) const;
protected:
  Pts _points;
  Pts _normals;
  scalarD _fcoef;
};
class MetricSCS : public Metric
{
public:
  MetricSCS(const Pts& points,const Pts& normals,scalar fcoef);
  virtual Cold solve(const Vec3d& extf,const Vec3d& extt) const override;
  virtual scalarD testSolution(const Vec3d& extf,const Vec3d& extt,const Cold& f) const override;
};

PRJ_END

#endif
