#include "ScsInterface.h"
#include "scs/glbopts.h"
#include "scs/util.h"
#include "amatrix.h"

USE_PRJ_NAMESPACE

ScsInterface::SMat toSparse(const Matd& coef,sizeType coff)
{
  ScsInterface::STrips trips;
  for(sizeType r=0; r<coef.rows(); r++)
    for(sizeType c=0; c<coef.cols(); c++)
      trips.push_back(ScsInterface::STrip(r,c+coff,coef(r,c)));
  ScsInterface::SMat ret;
  ret.resize(coef.rows(),coef.cols()+coff);
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
ScsInterface::SMat toSparse(const ScsInterface::SMat& coef,sizeType coff)
{
  ScsInterface::STrips trips;
  for(sizeType i=0; i<coef.outerSize(); i++)
    for(ScsInterface::SMat::InnerIterator it(coef,i); it; ++it)
      trips.push_back(ScsInterface::STrip(it.row(),it.col()+coff,it.value()));
  ScsInterface::SMat ret;
  ret.resize(coef.rows(),coef.cols()+coff);
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
ScsInterface::ScsInterface()
{
  _k = (ScsCone *)scs_calloc(1, sizeof(ScsCone));
  _d = (ScsData *)scs_calloc(1, sizeof(ScsData));
  _d->stgs = (ScsSettings *)scs_calloc(1, sizeof(ScsSettings));
  _sol = (ScsSolution *)scs_calloc(1, sizeof(ScsSolution));
}
ScsInterface::~ScsInterface()
{
  SCS(free_data)(_d, _k);
  SCS(free_sol)(_sol);
}
//b-A*x \in K
void ScsInterface::constraintLinear(const Matd& coef,sizeType off,const Vec& b,CONE_TYPE type)
{
  ASSERT(type == EQUALITY || type == INEQUALITY)
  ASSERT(coef.rows() == b.size())
  _cones[type].push_back(std::make_pair(toSparse(coef,off),b));
}
void ScsInterface::constraintLinear(const Vec& coef,sizeType off,scalarD b,CONE_TYPE type)
{
  FUNCTION_NOT_IMPLEMENTED
}
void ScsInterface::constraintSecondOrder(const Matd& coef,sizeType off,const Vec& b,CONE_TYPE type)
{
  ASSERT(type == SECOND_ORDER)
  ASSERT(coef.rows() == b.size())
  _cones[type].push_back(std::make_pair(toSparse(coef,off),b));
}
void ScsInterface::constraintSecondOrder(const SMat& coef,sizeType off,const Vec& b,CONE_TYPE type)
{
  ASSERT(type == SECOND_ORDER)
  ASSERT(coef.rows() == b.size())
  _cones[type].push_back(std::make_pair(toSparse(coef,off),b));
}
void ScsInterface::constraintLinearMatrixInequality(const Matd& coef,sizeType off,const Matd& c0,CONE_TYPE type)
{
  FUNCTION_NOT_IMPLEMENTED
}
sizeType ScsInterface::solve(const Vec& c,Vec& x,bool report)
{
  _k->f=_k->l=0;
  _k->q=NULL;
  _k->qsize=0;
  _k->s=NULL;
  _k->ssize=0;
  _k->ep=_k->ed=0;
  _k->p=NULL;

  STrips A;
  std::vector<scalarD> b;
  std::vector<scs_int> qarr;

  for(CONES::const_iterator it=_cones.begin(); it!=_cones.end(); ++it) {
    const std::vector<std::pair<SMat,Vec>>& cones=it->second;
    for(sizeType cid=0; cid<(sizeType)cones.size(); cid++) {
      const SMat& ABlk=cones[cid].first;
      const Vec& bBlk=cones[cid].second;
      //assemble A
      for(sizeType i=0; i<ABlk.outerSize(); i++)
        for(ScsInterface::SMat::InnerIterator it(ABlk,i); it; ++it)
          A.push_back(STrip(it.row()+b.size(),it.col(),it.value()));
      //assemble b
      for(sizeType i=0; i<bBlk.size(); i++)
        b.push_back(bBlk[i]);
      //assemble ScsCone
      switch(it->first) {
      case EQUALITY:
        _k->f+=bBlk.size();
        break;
      case INEQUALITY:
        _k->l+=bBlk.size();
        break;
      case SECOND_ORDER:
        qarr.push_back(bBlk.size());
        _k->qsize++;
        break;
      default:
        ASSERT_MSG(false,"Unknown cone type!")
      }
    }
  }
  if(_k->qsize>0) {
    _k->q=(scs_int*)scs_calloc(_k->qsize,sizeof(scs_int));
    memcpy(_k->q,&qarr[0],sizeof(scs_int)*_k->qsize);
  }

  _d->m=b.size();
  _d->n=x.size();

  SMat AM;
  AM.resize(_d->m,_d->n);
  AM.setFromTriplets(A.begin(),A.end());
  AM.makeCompressed();
  _d->A=(ScsMatrix*)scs_calloc(1,sizeof(ScsMatrix));
  _d->A->x=(scs_float*)scs_calloc(AM.nonZeros(),sizeof(scs_float));
  _d->A->i=(scs_int*)scs_calloc(AM.nonZeros(),sizeof(scs_int));
  _d->A->p=(scs_int*)scs_calloc(AM.cols()+1,sizeof(scs_int));
  _d->A->m=_d->m;
  _d->A->n=_d->n;
  Eigen::Map<Eigen::Matrix<scs_float,-1,1>>(_d->A->x,AM.nonZeros())=
      Eigen::Map<Eigen::Matrix<scalarD,-1,1>>(AM.valuePtr(),AM.nonZeros()).template cast<scs_float>();
  Eigen::Map<Eigen::Matrix<scs_int,-1,1>>(_d->A->i,AM.nonZeros())=
      Eigen::Map<Eigen::Matrix<sizeType,-1,1>>(AM.innerIndexPtr(),AM.nonZeros()).template cast<scs_int>();
  Eigen::Map<Eigen::Matrix<scs_int,-1,1>>(_d->A->p,AM.cols()+1)=
      Eigen::Map<Eigen::Matrix<sizeType,-1,1>>(AM.outerIndexPtr(),AM.cols()+1).template cast<scs_int>();

  _d->b=(scs_float*)scs_calloc(_d->m,sizeof(scs_float));
  _d->c=(scs_float*)scs_calloc(_d->n,sizeof(scs_float));
  Eigen::Map<Eigen::Matrix<scs_float,-1,1>>(_d->b,_d->m)=Eigen::Map<Eigen::Matrix<scalarD,-1,1>>(&b[0],_d->m).template cast<scs_float>();
  Eigen::Map<Eigen::Matrix<scs_float,-1,1>>(_d->c,_d->n)=c.template cast<scs_float>();
  scs_set_default_settings(_d);
  _d->stgs->verbose=report;

  ScsInfo info;
  scs(_d,_k,_sol,&info);
  x=Eigen::Map<Eigen::Matrix<scs_float,-1,1>>(_sol->x,_d->n).template cast<scalarD>();
  return info.status_val;
}
