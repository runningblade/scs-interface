#include "Metric.h"
#include "RotationUtil.h"
#include "ScsInterface.h"

USE_PRJ_NAMESPACE

Metric::Metric(const Pts& points,const Pts& normals,scalar fcoef):_points(points),_normals(normals),_fcoef(fcoef) {}
Cold Metric::solve(const Vec3d& extf,const Vec3d& extt) const
{
  std::vector<monty::rc_ptr<VariableM>> fss;
  monty::rc_ptr<ModelM> model=new ModelM;
  monty::rc_ptr<ExpressionM> fStacked;
  monty::rc_ptr<ExpressionM> sumOfF=MosekInterface::toMosekE(Cold(extf));
  monty::rc_ptr<ExpressionM> sumOfT=MosekInterface::toMosekE(Cold(extt));
  for(sizeType i=0; i<(sizeType)_points.size(); i++) {
    monty::rc_ptr<VariableM> f=model->variable("f"+std::to_string(i),3);
    monty::rc_ptr<MatrixM> cp=MosekInterface::toMosek(Matd(cross<scalarD>(_points[i])));
    fss.push_back(f);
    //frictional cone
    Mat3d m;
    m.row(0)=inNormal(i).transpose()*_fcoef;
    m.row(1)=tangent1(i).transpose();
    m.row(2)=tangent2(i).transpose();
    model->constraint("f"+std::to_string(i)+"Cone",ExprM::mul(MosekInterface::toMosek(Matd(m)),f),DomainM::inQCone());
    //sum of F
    sumOfF=ExprM::add(sumOfF,f);
    sumOfT=ExprM::add(sumOfT,ExprM::mul(cp,f));
  }
  model->constraint("sumOfF",sumOfF,DomainM::equalsTo(MosekInterface::toMosek(Cold(Cold::Zero(3)))));
  model->constraint("sumOfT",sumOfT,DomainM::equalsTo(MosekInterface::toMosek(Cold(Cold::Zero(3)))));
  //objective
  for(sizeType i=1; i<(sizeType)fss.size(); i++)
    if(i==1)
      fStacked=ExprM::vstack(fss[0],fss[i]);
    else fStacked=ExprM::vstack(fStacked,fss[i]);
  monty::rc_ptr<VariableM> t=model->variable("t",1);
  model->constraint("obj",ExprM::vstack(t,fStacked),DomainM::inQCone());
  model->objective(ObjectiveSenseM::Minimize,t);
  //solve
  std::string str;
  if(!MosekInterface::trySolve(*model,str))
    return Cold::Zero(0);
  Cold fssV=Cold::Zero((sizeType)_points.size()*3);
  for(sizeType i=0; i<(sizeType)_points.size(); i++) {
    std::shared_ptr<monty::ndarray<double,1>> vals=fss[i]->level();
    fssV.segment<3>(i*3)=Eigen::Map<Eigen::Matrix<double,3,1>>(vals->raw()).cast<scalarD>();
  }
  return fssV;
}
scalarD Metric::testSolution(const Vec3d& extf,const Vec3d& extt,const Cold& f) const
{
  if(f.size()==(sizeType)_points.size()*3) {
    INFO("Solve successful!")
    scalarD err=0;
    Vec3d sumOfF=extf,sumOfT=extt;
    for(sizeType i=0; i<(sizeType)_points.size(); i++) {
      Vec3d fi=f.template segment<3>(i*3);
      sumOfF+=fi;
      sumOfT+=_points[i].cross(fi);
      scalarD errFri=std::min<scalarD>(fi.dot(_normals[i])-(fi-fi.dot(_normals[i])*_normals[i]).norm(),0);
      INFOV("Err of frictional cone %ld: %lf",i,errFri)
      err=std::max<scalarD>(err,errFri);
    }

    scalarD errF=sumOfF.cwiseAbs().maxCoeff();
    INFOV("Err of extF: %lf",errF)
    err=std::max<scalarD>(errF,err);

    scalarD errT=sumOfT.cwiseAbs().maxCoeff();
    INFOV("Err of extT: %lf",errT)
    err=std::max<scalarD>(errT,err);
    return err;
  } else if(f.size()==0) {
    INFO("Solve failed!")
    return -1;
  } else {
    INFO("Unknown size of solution!")
    return -2;
  }
}
const Vec3d Metric::inNormal(sizeType id) const
{
  return _normals[id];
}
const Vec3d Metric::tangent1(sizeType id) const
{
  sizeType minId;
  inNormal(id).cwiseAbs().minCoeff(&minId);
  return inNormal(id).cross(Vec3d::Unit(minId)).normalized();
}
const Vec3d Metric::tangent2(sizeType id) const
{
  return inNormal(id).cross(tangent1(id));
}

MetricSCS::MetricSCS(const Pts& points,const Pts& normals,scalar fcoef):Metric(points,normals,fcoef) {}
Cold MetricSCS::solve(const Vec3d& extf,const Vec3d& extt) const
{
  //<f,t>
  sizeType nrVar=(sizeType)_points.size()*3+1;
  Cold x=Cold::Zero(nrVar);
  Cold c=Cold::Zero(nrVar);
  c[c.size()-1]=1;

  ScsInterface scs;
  Mat3Xd sumf=Mat3Xd::Zero(3,nrVar-1),sumt=sumf;
  for(sizeType i=0; i<(sizeType)_points.size(); i++) {
    //frictional cone
    Mat3d m;
    m.row(0)=inNormal(i).transpose()*_fcoef;
    m.row(1)=tangent1(i).transpose();
    m.row(2)=tangent2(i).transpose();
    scs.constraintSecondOrder(-m,i*3,Vec3d::Zero(),ScsInterface::SECOND_ORDER);
    //sumf
    sumf.block<3,3>(0,i*3)=Mat3d::Identity()*-1;
    //sumt
    sumt.block<3,3>(0,i*3)=cross<scalarD>(_points[i])*-1;
  }
  //sumf
  scs.constraintLinear(sumf,0,extf,ScsInterface::EQUALITY);
  scs.constraintLinear(sumt,0,extt,ScsInterface::EQUALITY);
  //objective
  ScsInterface::SMat objCone;
  objCone.resize(nrVar,nrVar);
  objCone.coeffRef(0,nrVar-1)=-1;
  for(sizeType i=0; i<(sizeType)_points.size()*3; i++)
    objCone.coeffRef(i+1,i)=-1;
  scs.constraintSecondOrder(objCone,0,Cold::Zero(nrVar),ScsInterface::SECOND_ORDER);
  //solve
  if(scs.solve(c,x)>=0)
    return x.segment(0,nrVar-1);
  else return Cold::Zero(0);
}
scalarD MetricSCS::testSolution(const Vec3d& extf,const Vec3d& extt,const Cold& f) const
{
  scalarD ret=Metric::testSolution(extf,extt,f);
  if(ret>=0) {
    Cold fMosek=Metric::solve(extf,extt);
    INFOV("Mosek SCS difference: %f",(f-fMosek).cwiseAbs().maxCoeff())
    ret=std::max<scalarD>(ret,(f-fMosek).cwiseAbs().maxCoeff());
  }
  return ret;
}
